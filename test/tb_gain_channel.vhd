library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.env.all;

library vunit_lib;
context vunit_lib.vunit_context;

library gain_lib;

entity tb_gain_channel is
	generic (
		runner_cfg   : string := runner_cfg_default;
		NUM_TESTDATA : integer;
		GAIN         : integer
	);
end entity tb_gain_channel;

architecture RTL of tb_gain_channel is
	
	signal clk              : std_logic := '0';
	
	signal generator_enable : boolean := false;
	signal checker_done     : boolean := false;
	
	signal generator_gain   : std_logic_vector(2 downto 0);	
	signal generator_valid  : std_logic;
	signal generator_data   : std_logic_vector(7 downto 0);
	signal checker_valid    : std_logic;
	signal checker_data     : std_logic_vector(7 downto 0);
	
begin

	clk_proc : process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process;

	test_runner : process
	begin
		report "Start Gain Processing Test";
		test_runner_setup(runner, runner_cfg);		
		wait for 100 ns;		
		generator_enable <= true;
		wait until checker_done;	
		report "Test done.";
		test_runner_cleanup(runner);
	end process;
	
	assert GAIN >= 0
		report "Gain must be larger than zero!"
		severity failure;
	
	generator_proc : process
		variable i : unsigned(generator_data'range) := (others => '0');
	begin
		
		generator_gain  <= std_logic_vector(to_unsigned(GAIN, generator_gain'length));
		generator_valid <= '0';
		
		if (not generator_enable) then
			wait until generator_enable;
		end if;
		
		report "Generator Enabled";
		
		while true loop		
			wait until rising_edge(clk);
			generator_valid <= '1';
			generator_data  <= std_logic_vector(i);
			i := i + 1;
		end loop;
		
		wait until rising_edge(clk);
		generator_valid <= '0';
		wait;
		
	end process;
	
	uut : entity gain_lib.gain
		generic map (
			DATA_WIDTH => generator_data'length
		)
		port map(
			clk_in   => clk,
			gain_in  => generator_gain,
			valid_in => generator_valid,
			data_in  => generator_data,
			valid_q  => checker_valid,
			data_q   => checker_data
		);
		
	checker_proc : process
		variable i        : integer                        := 0;
		variable exp_data : unsigned(generator_data'range) := (others => '0');
	begin
		
		checker_done <= false;

		-- Don't start checker before generator is active
		if (not generator_enable) then
			wait until generator_enable;
		end if;
		
		while(i < NUM_TESTDATA) loop		
			
			wait until rising_edge(clk);
			
			if (checker_valid = '1') then				
				assert unsigned(checker_data) = exp_data
					report "Data mismatch: " & integer'image(to_integer(unsigned(checker_data))) & " vs. " & integer'image(to_integer(exp_data))
					severity error;					
				exp_data := exp_data + 1;	
				i        := i + 1;			
			else				
				assert unsigned(checker_data) = 0 severity error;				
			end if;

		end loop;
		
		checker_done <= true;
		wait;
				
	end process;

end architecture RTL;
