from vunit import VUnit

# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create libraries
# UUT
gain_lib = vu.add_library("gain_lib")
gain_lib.add_source_files("src/*.vhd")

# Testbenches
test_lib = vu.add_library("test_lib")
test_lib.add_source_files("test/*.vhd")
tb_gain_channel = test_lib.entity("tb_gain_channel")

# Configurations
tb_gain_channel.add_config(
    name="No Gain",
    generics=dict(
        NUM_TESTDATA=1000,
        GAIN=1))

tb_gain_channel.add_config(
    name="Gain 2",
    generics=dict(
        NUM_TESTDATA=1000,
        GAIN=2))

# Run vunit function
vu.main()

