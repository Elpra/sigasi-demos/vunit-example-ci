-- Use gain values between 0 and 2, with 0.5 step width

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gain is
	generic(
		DATA_WIDTH : natural
	);
	port(
		clk_in   : in  std_logic;
		gain_in  : in  std_logic_vector(2 downto 0);
		valid_in : in  std_logic;
		data_in  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
		valid_q  : out std_logic;
		data_q   : out std_logic_vector(DATA_WIDTH-1 downto 0)
	);
end entity gain;

architecture RTL of gain is
	
	signal valid : std_logic;
	signal data  : unsigned(data_q'range);
	
begin

	valid_q <= valid;
	data_q  <= std_logic_vector(data);

	gain_proc : process (clk_in)
	begin
		
		if (rising_edge(clk_in)) then
			
			if (valid_in = '1') then
				valid <= '1';
				data  <= resize(unsigned(data_in) * unsigned(gain_in), data'length);
			else
				valid <= '0';
				data  <= (others => '0');
			end if;
			
		end if;
		
	end process;

end architecture RTL;
