-- Gain on RGB color, with 8 bit each.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rgb_gain is
	generic(
		CHANNEL_WIDTH : integer
	);
	port(
		clk_in   : in  std_logic;
		gain_in  : in  std_logic_vector( 8 downto 0);
		valid_in : in  std_logic;
		rgb_in   : in  std_logic_vector(3*CHANNEL_WIDTH-1 downto 0);
		valid_q  : out std_logic;
		rgb_q    : out std_logic_vector(3*CHANNEL_WIDTH-1 downto 0)
	);
end entity rgb_gain;

architecture RTL of rgb_gain is
	
	signal valid : std_logic_vector(2 downto 0);
	
begin

	valid_q <= '1' when valid = "111" else '0';
	
	gen_gain_channels : for i in 0 to 2 generate
	begin
	
		u_gain : entity work.gain
		generic map (
			DATA_WIDTH => CHANNEL_WIDTH
		)
		port map(
			clk_in   => clk_in,
			gain_in  => gain_in(3*i+2 downto 3*i),
			valid_in => valid_in,
			data_in  => rgb_in(CHANNEL_WIDTH*(i+1)-1 downto 8*i),
			valid_q  => valid(i),
			data_q   => rgb_q(CHANNEL_WIDTH*(i+1)-1 downto 8*i)
		);
	
	end generate;

end architecture RTL;
